import axios from "axios";

export const searchByWord = async (word) => {
    return await axios.get(`https://images-api.nasa.gov/search?q=${word}`);
}