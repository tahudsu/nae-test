import React, { useState } from 'react'
import PropTypes from 'prop-types';

export const Search = ({ change }) => {
    const initialState = '';
    const [search, setSearch] = useState(initialState);

    const handleChange = (e) => {
        setSearch(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        console.log(search);
        change(search);
        setSearch(initialState);
    }


    return (
        <div className="list-search__form">
            <form onSubmit={ handleSubmit } 
                >
                <input placeholder="saerch by word" type="text" value={ search }
                onChange={ handleChange }/>
                <button type="submit">Search</button>
            </form>
        </div>
    )
}

Search.propTypes = {
    search: PropTypes.func.isRequired
}