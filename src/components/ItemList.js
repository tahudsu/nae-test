import React from 'react'
import PropTypes from 'prop-types';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import './css/ItemList.css';

export const ItemList = ({ index, item}) => {
    const showImage = () => {
        Swal.fire(
            item.data[0].title,
            `<img src="${item.links[0].href}"></img>`)
    }

    return (
        <div key={ index } className="list-search__item">
            <h3>{ index+1 }. { item.data[0].title }</h3>
            <span>{ item.data[0].description}</span>
            { item.links?.length &&
                <button onClick={ showImage }>view image</button>
            }
        </div>
    )
}


ItemList.propTypes = {
    index: PropTypes.number.isRequired,
    item: PropTypes.object.isRequired
}