import React, { useEffect, useState } from 'react'
import { searchByWord } from '../api/nasa'
import { Results } from './Results';
import { Search } from './Search'

export const List = () => {
    const initialState = '';
    const [word, setWord] = useState(initialState);
    const [results, setResults] = useState([]);

    useEffect(() => {
        search(word);
        
        return () => {
        }
    }, [word])

    const changeWord = (word) => {
        setWord(word)
    }

    const search = async (word) => {
        if (word) {
            const res = await searchByWord(word).catch(err => {
                console.log(err);
            });
            console.log(res);
            setResults(res.data.collection.items);
        }
    }

    return (
        <div class="list-search">
            <h1 className="list-search__title">
                ListComponent: show word : { word }
            </h1>
            <Search change={ changeWord }/>
            ListComponent: show word : { word }
            <Results results={ results } />
        </div>
    )
}
