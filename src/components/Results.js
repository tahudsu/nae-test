import React from 'react'
import { ItemList } from './ItemList';

export const Results = ({ results }) => {
    
    return (
        <div className="list-search__list">
             {results.map((item, i) => {
                 console.log(item);

                 return (<ItemList key={i} index={i} item={item} />)
            }) 
            }
        </div>
    )
}
